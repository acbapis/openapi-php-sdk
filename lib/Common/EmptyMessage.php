<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: common/common.proto

namespace Common;

use Google\Protobuf\Internal\GPBType;
use Google\Protobuf\Internal\RepeatedField;
use Google\Protobuf\Internal\GPBUtil;

/**
 * EmptyMessage is a substitue for google.protobuf.Empty
 * to be used because there are language where Empty is
 * a reserved word (ex. PHP)
 *
 * Generated from protobuf message <code>common.EmptyMessage</code>
 */
class EmptyMessage extends \Google\Protobuf\Internal\Message
{

    /**
     * Constructor.
     *
     * @param array $data {
     *     Optional. Data for populating the Message object.
     *
     * }
     */
    public function __construct($data = NULL) {
        \GPBMetadata\Common\Common::initOnce();
        parent::__construct($data);
    }

}

