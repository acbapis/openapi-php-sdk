<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: openapi/openapi.proto

namespace Openapi;

use Google\Protobuf\Internal\GPBType;
use Google\Protobuf\Internal\RepeatedField;
use Google\Protobuf\Internal\GPBUtil;

/**
 * Generated from protobuf message <code>openapi.WeekArray</code>
 */
class WeekArray extends \Google\Protobuf\Internal\Message
{
    /**
     * Generated from protobuf field <code>repeated .openapi.Week weeks = 1;</code>
     */
    private $weeks;
    /**
     * Generated from protobuf field <code>int32 total = 2;</code>
     */
    private $total = 0;

    /**
     * Constructor.
     *
     * @param array $data {
     *     Optional. Data for populating the Message object.
     *
     *     @type \Openapi\Week[]|\Google\Protobuf\Internal\RepeatedField $weeks
     *     @type int $total
     * }
     */
    public function __construct($data = NULL) {
        \GPBMetadata\Openapi\Openapi::initOnce();
        parent::__construct($data);
    }

    /**
     * Generated from protobuf field <code>repeated .openapi.Week weeks = 1;</code>
     * @return \Google\Protobuf\Internal\RepeatedField
     */
    public function getWeeks()
    {
        return $this->weeks;
    }

    /**
     * Generated from protobuf field <code>repeated .openapi.Week weeks = 1;</code>
     * @param \Openapi\Week[]|\Google\Protobuf\Internal\RepeatedField $var
     * @return $this
     */
    public function setWeeks($var)
    {
        $arr = GPBUtil::checkRepeatedField($var, \Google\Protobuf\Internal\GPBType::MESSAGE, \Openapi\Week::class);
        $this->weeks = $arr;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int32 total = 2;</code>
     * @return int
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Generated from protobuf field <code>int32 total = 2;</code>
     * @param int $var
     * @return $this
     */
    public function setTotal($var)
    {
        GPBUtil::checkInt32($var);
        $this->total = $var;

        return $this;
    }

}

