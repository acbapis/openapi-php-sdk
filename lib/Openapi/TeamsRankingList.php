<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: openapi/openapi.proto

namespace Openapi;

use Google\Protobuf\Internal\GPBType;
use Google\Protobuf\Internal\RepeatedField;
use Google\Protobuf\Internal\GPBUtil;

/**
 * Generated from protobuf message <code>openapi.TeamsRankingList</code>
 */
class TeamsRankingList extends \Google\Protobuf\Internal\Message
{
    /**
     * Generated from protobuf field <code>repeated .openapi.TeamsRankingListItem items = 1;</code>
     */
    private $items;

    /**
     * Constructor.
     *
     * @param array $data {
     *     Optional. Data for populating the Message object.
     *
     *     @type \Openapi\TeamsRankingListItem[]|\Google\Protobuf\Internal\RepeatedField $items
     * }
     */
    public function __construct($data = NULL) {
        \GPBMetadata\Openapi\Openapi::initOnce();
        parent::__construct($data);
    }

    /**
     * Generated from protobuf field <code>repeated .openapi.TeamsRankingListItem items = 1;</code>
     * @return \Google\Protobuf\Internal\RepeatedField
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * Generated from protobuf field <code>repeated .openapi.TeamsRankingListItem items = 1;</code>
     * @param \Openapi\TeamsRankingListItem[]|\Google\Protobuf\Internal\RepeatedField $var
     * @return $this
     */
    public function setItems($var)
    {
        $arr = GPBUtil::checkRepeatedField($var, \Google\Protobuf\Internal\GPBType::MESSAGE, \Openapi\TeamsRankingListItem::class);
        $this->items = $arr;

        return $this;
    }

}

