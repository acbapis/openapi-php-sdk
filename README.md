# ACB APIs PHP SDK

This repository contains the PHP SDK that allows you to connect with ACB APIs via [gRPC](https://grpc.io/)

## Installation

The SDK can be installed with [Composer](https://getcomposer.org)

First, add the BitBucket repository to the ``composer.json`` file:

```
{
    "name": "myProject",
    ...
    "repositories": [
        {
            "type": "vcs",
            "url": "https://bitbucket.org/acbapis/openapi-php-sdk"
        }
    ],
    ...
}

```

Then, add the dependency:

```
composer require acbapis/openapi-php-sdk 0.5.*
```

## Usage

This example shows how to open a connection with OpenAPI and obtain a list of competitions:

```
#!php
<?php

require dirname(__FILE__).'/vendor/autoload.php';

use Openapi\OpenApiServiceClient;

$server = "openapi.devapis.acb.info:59800";
// Open connection
$client = new OpenApiServiceClient($server, [
    "credentials" => Grpc\ChannelCredentials::createInsecure(),
]);

$filter = new Openapi\CompetitionFilter();
// Allows pagination
$filter->setLimit(10);

list($competitions, $status) = $client->ListCompetitions($filter)->wait();

printf("There are %d competitions\n", $competitions->getTotal());

// Iterate over results
foreach ($competitions->getCompetitions() as $item) {
    // Access competition's fields
    printf(
        "Competition: %20s\t (%6s )\tStatus: %d\n",
        $item->getCompetitionStr(),
        $item->getCompetitionAbbrev(),
        $item->getStatus()
    );
}

// Close the connection
$client->close();
```

## Streaming calls
This example shows how to make a streaming call to the ``StreamPartidos`` endpoint
of Competicion service:
```
#!php
<?php

<?php

require dirname(__FILE__).'/vendor/autoload.php';

use Competicion\CompeticionServiceClient;
use Competicion\PartidoFilter;

$server = "competicion.devapis.acb.info:59800";

// Open connection
$client = new CompeticionServiceClient($server, [
    "credentials" => Grpc\ChannelCredentials::createInsecure(),
]);

$partidoFilter = new PartidoFilter();
// Only return 'id' field
$partidoFilter->setFields("id");

$call = $client->StreamPartidos($partidoFilter);
$partidos = $call->responses();
// Iterate over server responses
foreach ($partidos as $partido) {
    printf(
        "- %s\n",
        $partido->getId()
    );
    // The loop will continue until the server indicates there are no more responses
}

// Close the connection
$client->close();

```

## Common parameters

### Pagination

Where aplicable, API calls can be paginated using the ``offset`` and ``limit`` parameters.
``offset`` sets the first result to be returned, and starts at zero.
``limit`` allows to select the number of items to be returned. In general, the maximum
limit allowed is ``50``.

### Field selection

Some API calls allow to reduce the amount of data returned by specifying the fields that
will be returned. Fields are specified in a comma-separated list of fields names, where
nested field follow the structure ``{{parent field}}.{{child field}}``

For example, for selecting the id, shirt number and name of a ``License``, the ``fields``
parameter should contain: ``id,shirtNumber,person.name``
