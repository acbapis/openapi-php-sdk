<?php

require dirname(__FILE__).'/vendor/autoload.php';

$server = "oa.dev.acbdata.net:59800";
$client = new \Openapi\OpenApiServiceClient($server, [
    "credentials" => \Grpc\ChannelCredentials::createInsecure(),
]);

// Obtener todos los datos del partido
$findByIdFilter = new \Openapi\FindMatchByIdFilter();
$findByIdFilter->setId(18126);
$findByIdFilter->setFullStats(true);

list($match, $status) = $client->FindMatchByID($findByIdFilter)->wait();

echo "Estadísticas partido ID: " . $match->getId() . "\n";

foreach ($match->getTeams() as $team) {
        echo $team->getInfo()->getTeamStr() . ":\n";
        echo "Puntos: " . $team->getScore() . "\n";
        echo "BoxScore:\n";
        echo "Jugador Puntos Valoración\n";
        foreach ($team->getBoxScore()->getStatsLines() as $statsLine) {
                $jugador = ($statsLine->getLicenseStr()) ? $statsLine->getLicenseStr() : "EQUIPO";
                $puntos = ($statsLine->getTotalScore() !== "") ? $statsLine->getTotalScore() : "_";
                $valoracion = ($statsLine->getEfficiency() !== "") ? $statsLine->getEfficiency() : "_";
                echo "$jugador : $puntos $valoracion\n";
        }

        echo "\n";
}

// Close the connection
$client->close();
