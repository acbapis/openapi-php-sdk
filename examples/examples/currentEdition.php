<?php

require dirname(__FILE__).'/vendor/autoload.php';

$server = "oa.dev.acbdata.net:59800";
$client = new \Openapi\OpenApiServiceClient($server, [
    "credentials" => \Grpc\ChannelCredentials::createInsecure(),
]);

$idFilter = new \Common\IdMessage();
$idFilter->setId(2);

list($edition, $status) = $client->FindEditionByCompetition($idFilter)->wait();

echo "Edición actual de Copa del Rey:\n";
echo "[" . $edition->getId() . "] " . $edition->getEditionStr() . "\n";

// Close the connection
$client->close();
