<?php

require dirname(__FILE__).'/vendor/autoload.php';

$server = "oa.dev.acbdata.net:59800";
$client = new \Openapi\OpenApiServiceClient($server, [
    "credentials" => \Grpc\ChannelCredentials::createInsecure(),
]);

// Búsqueda por ID de jornada
$standingFilter = new \Openapi\StandingFilter();
$standingFilter->setWeekId(2710);

list($standings, $status) = $client->ListStandings($standingFilter)->wait();

echo "Clasificación Jornada ID 2710:\n";
// $standings->getStandings() devuelve un array de clasificaciones. Para iterar
// sobre los equipos, hay que iterar sobre el resultado también
foreach ($standings->getStandings() as $standing) {
        foreach ($standing->getStandings() as $team) {
                echo $team->getPosition() . "º: " . $team->getTeamName() . " (" . $team->getWon() . "-" . $team->getLost() . ")\n";
        }
}

// Búsqueda por edición y nº de jornada
$standingFilter = new \Openapi\StandingFilter();
$standingFilter->setEditionID(920);
$standingFilter->setWeekNum(34);

list($standings, $status) = $client->ListStandings($standingFilter)->wait();

echo "\nClasificación Jornada 34º de la Edición ID 920:\n";
// $standings->getStandings() devuelve un array de clasificaciones. Para iterar
// sobre los equipos, hay que iterar sobre el resultado también
foreach ($standings->getStandings() as $standing) {
        foreach ($standing->getStandings() as $team) {
                echo $team->getPosition() . "º: " . $team->getTeamName() . " (" . $team->getWon() . "-" . $team->getLost() . ")\n";
        }
}

// Close the connection
$client->close();
