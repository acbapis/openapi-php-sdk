<?php

require dirname(__FILE__).'/vendor/autoload.php';

$server = "oa.dev.acbdata.net:59800";
$client = new \Openapi\OpenApiServiceClient($server, [
    "credentials" => \Grpc\ChannelCredentials::createInsecure(),
]);

// Calendario de la edición nº 62 de Liga ENDESA
$scheduleFilter = new \Openapi\ScheduleFilter();
$scheduleFilter->setEditionNum(62);
$scheduleFilter->setCompetitionId(1);
list($schedule, $status) = $client->GetSchedule($scheduleFilter)->wait();

echo "Calendario edición " . $schedule->getEditionNum() . "º " . $schedule->getCompetitionStr() . "\n";
foreach ($schedule->getWeeks() as $week) {
        $desde = date("d-m-Y", $week->getStart());
        $hasta = date("d-m-Y", $week->getEnd());
        echo "Jornada " . $week->getWeekNum() . ": " . $desde . " -> " . $hasta . "\n";
}

// Calendario de la edición nº 82 de Copa
$scheduleFilter = new \Openapi\ScheduleFilter();
$scheduleFilter->setComped("C17");
list($schedule, $status) = $client->GetSchedule($scheduleFilter)->wait();

echo "\nCalendario edición " . $schedule->getEditionNum() . "º " . $schedule->getCompetitionStr() . "\n";
foreach ($schedule->getWeeks() as $week) {
        $desde = date("d-m-Y", $week->getStart());
        $hasta = date("d-m-Y", $week->getEnd());
        echo "Jornada " . $week->getWeekNum() . ": " . $desde . " -> " . $hasta . "\n";
}

// Close the connection
$client->close();
