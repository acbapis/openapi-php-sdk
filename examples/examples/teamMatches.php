<?php

require dirname(__FILE__).'/vendor/autoload.php';

$server = "oa.dev.acbdata.net:59800";
$client = new \Openapi\OpenApiServiceClient($server, [
    "credentials" => \Grpc\ChannelCredentials::createInsecure(),
]);

$matchFilter = new \Openapi\MatchFilter();
$matchFilter->setNext(true);
$matchFilter->setTeamId(9);

list($matches, $status) = $client->ListMatches($matchFilter)->wait();

echo "Próximos partido Real Madrid:\n";
foreach ($matches->getMatches() as $match) {
        $local = $match->getLocalTeam();
        $visitante = $match->getVisitingTeam();
        $fecha = date("d-m-Y H:i", $match->getStartDate());
        echo "$fecha : $local - $visitante\n";
}

// Close the connection
$client->close();
