<?php

require dirname(__FILE__).'/vendor/autoload.php';

$server = "oa.dev.acbdata.net:59800";
$client = new \Openapi\OpenApiServiceClient($server, [
    "credentials" => \Grpc\ChannelCredentials::createInsecure(),
]);

// Obtener noticias con un resumen
$newsFilter = new \Openapi\NewsFilter();
list($news, $status) = $client->ListNews($newsFilter)->wait();

foreach ($news->getNews() as $item) {
        echo $item->getTitle() . "\n\n";
        echo $item->getSummary() . "\n";
        echo "----------------" . "\n";
}

// Obtener noticias junto con su contenido
$newsFilter = new \Openapi\NewsFilter();
$newsFilter->setAddContent(true);
list($news, $status) = $client->ListNews($newsFilter)->wait();

foreach ($news->getNews() as $item) {
        echo $item->getTitle() . "\n\n";
        echo $item->getContent() . "\n";
        echo "----------------" . "\n";
}

// Close the connection
$client->close();
