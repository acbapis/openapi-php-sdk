<?php

require dirname(__FILE__).'/vendor/autoload.php';

$server = "oa.dev.acbdata.net:59800";
$client = new \Openapi\OpenApiServiceClient($server, [
    "credentials" => \Grpc\ChannelCredentials::createInsecure(),
]);

// Búsqueda por ID de edición
$idFilter = new \Common\IdMessage();
$idFilter->setId(20204097);

list($profile, $status) = $client->GetPlayerProfile($idFilter)->wait();

echo "ID: " . $profile->getId() . "\n";
echo "Nombre: " . $profile->getName() . "\n";
echo "Abreviatura: " . $profile->getAbbrev() . "\n";
echo "Nacimiento: " . $profile->getDateOfBirth() . " (" . $profile->getPlaceOfBirth() . ")\n";
echo "Foto: " . $profile->getPhoto() . "\n";
echo "Posición: " . $profile->getPosition() . "\n";
echo "Dorsal: " . $profile->getShirtNumber() . "\n";

// Close the connection
$client->close();
