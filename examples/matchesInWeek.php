<?php

require dirname(__FILE__).'/vendor/autoload.php';

$server = "oa.dev.acbdata.net:59800";
$client = new \Openapi\OpenApiServiceClient($server, [
    "credentials" => \Grpc\ChannelCredentials::createInsecure(),
]);

$weeksFilter = new \Openapi\FindByIdFilter();
$weeksFilter->setId(2677);
list($week, $status) = $client->FindWeekById($weeksFilter)->wait();

echo "Jornada nº" . $week->getWeekNum() . " de " . $week->getCompetitionStr() . "\n";
echo "Partidos:\n";
foreach ($week->getMatches() as $match) {
        echo $match->getMatchNum() . "º " . $match->getLocalTeam() . "-" . $match->getVisitingTeam() . "\n";
}

// Close the connection
$client->close();
