<?php

require dirname(__FILE__).'/vendor/autoload.php';

$server = "oa.dev.acbdata.net:59800";
$client = new \Openapi\OpenApiServiceClient($server, [
    "credentials" => \Grpc\ChannelCredentials::createInsecure(),
]);

// Búsqueda por ID de edición
$teamFilter = new \Openapi\TeamFilter();
$teamFilter->setEditionId(920);

list($teams, $status) = $client->ListTeams($teamFilter)->wait();

echo "Equipos edición ID 920:\n";

foreach ($teams->getTeams() as $team) {
        echo "- " . $team->getTeamStr() . "\n";
}

// Búsqueda por competición y nº de edición
$teamFilter = new \Openapi\TeamFilter();
$teamFilter->setCompetitionStr("Liga Nacional");
$teamFilter->setEditionNum(62);

list($standings, $status) = $client->ListTeams($teamFilter)->wait();

echo "\nEquipos edición nº 62 de Liga Nacional:\n";

foreach ($teams->getTeams() as $team) {
        echo "- " . $team->getTeamStr() . "\n";
}
// Close the connection
$client->close();
